# InfiniteJumping
This is a SourceMod plugin it lets users auto jump and/or double jump in mid air, when holding down the jump button.
AlliedModders Thread: https://forums.alliedmods.net/showthread.php?t=132391

## Dependency

[SMLib](https://gitlab.com/counterstrikesource/sm-plugins/smlib)
